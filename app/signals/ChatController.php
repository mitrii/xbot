<?php

namespace app\signals;

use app\models\Poll;
use app\models\PollMember;
use mitrii\bot\jobs\UpdateJob;
//use Spatie\Emoji\Emoji;
use mitrii\bot\View;
use unreal4u\TelegramAPI\Telegram\Methods\SendMessage;
use unreal4u\TelegramAPI\Telegram\Types\Inline\Keyboard\Markup;
use unreal4u\TelegramAPI\Telegram\Types\KeyboardButton;
use Yii;

class ChatController extends \mitrii\bot\Controller
{


    public function actionIndex()
    {
        return '';
    }


    public function actionHelp()
    {
        return Yii::t('app', 'Run /help command');
    }

    public function actionMinus()
    {

    }

    public function actionDelay()
    {
        \Yii::$app->queue->delay(30)->push(new UpdateJob([
            'update' => \Yii::$app->getRequest()->update,
            'route' => ['chat/delayed', []]
        ]));
        return 'send message "delayed" after 30 seconds';
    }

    public function actionDelayed()
    {
        return 'delayed';
    }

    public function actionKeyboard()
    {
        $this->view->markup_type = View::MT_KEYBOARD;
        $this->view->keyboard_columns = 1;
        $this->view->addKeyboardButton(['text' => 'Первая кнопка']);
        $this->view->addKeyboardButton(['text' => 'Вторая кнопка', 'request_location' => true]);

        return 'test keyboard';
    }

    public function actionInlineButton()
    {
        $this->view->markup_type = View::MT_INLINE_KEYBOARD;
        $this->view->keyboard_columns = 2;
        $this->view->addKeyboardButton(['text' => 'Кнопка 1', 'callback_data' => ['route' => 'chat/callback', 'params' => ['id' => 1]]]);
        $this->view->addKeyboardButton(['text' => 'Кнопка 2', 'callback_data' => ['route' => 'chat/callback', 'params' => ['id' => 2]]]);
        $this->view->addKeyboardButton(['text' => 'Кнопка 3', 'callback_data' => ['route' => 'chat/callback', 'params' => []]]);
        return 'Приветики';
    }

    public function actionCallback($id)
    {
        return 'callback : ' . $id;
    }

    public function actionReply()
    {
        Yii::$app->response->isReply = true;
        return 'what?';
    }

    public function actionSendUserId()
    {
        return Yii::$app->user->getId();
    }

    public function actionCheckAccess()
    {
        return 'status: ' . (Yii::$app->user->can('test')) ? 'access granted' : 'access denied';
    }

}
