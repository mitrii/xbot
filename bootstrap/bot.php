<?php

/*
 * --------------------------------------------------------------------------
 * Register auto loaders
 * --------------------------------------------------------------------------
 *
 * Add registered class loaders required for our application.
 *
 */

require(__DIR__ . '/autoload.php');

/*
 * --------------------------------------------------------------------------
 * Initialize SideKit library
 * --------------------------------------------------------------------------
 *
 * This step is required *prior* adding the application script.
 *
 */

require(__DIR__ . '/sidekit.php');

/*
 * --------------------------------------------------------------------------
 * Initialize custom aliases
 * --------------------------------------------------------------------------
 *
 * Add custom aliases to the application. Added after sidekit to take
 * advantage of its loaded configuration values
 */

require(__DIR__ . '/aliases.php');


use SideKit\Config\ConfigKit;
use mitrii\bot\Application;

/*
 * --------------------------------------------------------------------------
 * Create Yii console application
 * --------------------------------------------------------------------------
 *
 * Grab the configuration for the 'console' application and initialize an
 * Application instance with it. Remember that the file 'sidekit.php' has to
 * be *required* prior inserting this script so we can access the environment
 * variables.
 */

$config = ConfigKit::config()->build('bot', ConfigKit::env()->get('CONFIG_USE_CACHE'));

$app = new Application($config);

/*
 * --------------------------------------------------------------------------
 * Return application
 * --------------------------------------------------------------------------
 *
 * Return instance to calling script, that way we separate initialization
 * processes and liberate the entry script with too much code.
 */

return $app;
