<?php

use SideKit\Config\ConfigKit;

return [
    'class' => \mitrii\bot\BotManager::class,
    'botApp' => __DIR__ . '/../../../bootstrap/bot.php',
    'apiToken' => ConfigKit::env()->get('BOT_API_TOKEN'),
];