<?php

use SideKit\Config\ConfigKit;

return [
    'class' => \mitrii\bot\BotManager::class,
    'apiToken' => ConfigKit::env()->get('BOT_API_TOKEN'),
];