<?php

return [
    'class' => mitrii\bot\SignalManager::className(),
    'rules' => [
        '/new' => 'chat/new',
        '/+' => 'chat/plus',
        '+' => 'chat/plus',
        '/-' => 'chat/minus',
        '-' => 'chat/minus',
        '/help' => 'chat/help',
        //\Spatie\Emoji\Emoji::tableTennisPaddleAndBall() => 'chat/tennis',
        '/stop' => 'chat/stop',
        'delay' => 'chat/delay',
        'keyboard' => 'chat/keyboard',
        'reply' => 'chat/reply',
        '/button' => 'chat/inline-button',
        '/getuserid' => 'chat/send-user-id',
        '/getaccess' => 'chat/check-access',
    ]
];