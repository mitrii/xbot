<?php

return [
    'class' => \yii\queue\redis\Queue::class,
    'redis' => 'queueRedis', // connection ID
    'channel' => 'updates', // queue channel
];